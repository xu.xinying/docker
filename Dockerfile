FROM 192.168.1.101:1180/anicloud/openjdk:8
MAINTAINER xxy.ninnjia.com
VOLUME ["/tmp"]
COPY target/docker-0.0.1.jar /docker.jar
#ENTRYPOINT ["java","-jar","/docker.jar","--spring.profiles.active=sandbox"]
ENTRYPOINT ["java","-jar","/docker.jar"]