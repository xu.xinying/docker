#！/usr/bin/env bash

#mvn package -Dmaven.test.skip=true -P dev
mvn package -Dmaven.test.skip=true

docker build -t 192.168.1.101:1180/anicloud/docker:latest .
docker push 192.168.1.101:1180/anicloud/docker:latest

appName="docker"
word="1"
echo "$word"
word=`docker ps -a -q --no-trunc --filter name="$appName"`
echo "$word"
if [ -z "$word" ];
then
    echo "当前不存在该容器-------------------------------------"
elif [ -n "$word" ];
then
    echo "当前已存在容器，停止并移除该容器-------------------------------------"
    /usr/bin/docker stop "$word"
    /usr/bin/docker rm "$word"
elif [ "$word" == "1" ];
then
    echo "查询的信息有误，执行默认操作-------------------------------------"
    /usr/bin/docker stop "$word"
    /usr/bin/docker rm "$word"
fi

echo "启动容器-------------------------------------"
docker-compose up -d
echo "Remove dangling images-------------------------------------"
docker image prune -f
