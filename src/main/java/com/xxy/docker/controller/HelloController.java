package com.xxy.docker.controller;


import com.xxy.base.service.BaseService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/hello")
public class HelloController {
    private final static Logger logger = LoggerFactory.getLogger(HelloController.class);

    @Value("${hello.env}")
    private String environment;

    @GetMapping("/docker")
    public String helloDocker(){
        BaseService baseService = new BaseService();
        logger.debug("hello i am a logger debug");
        return baseService.getRainbow()+"彩虹 welcome jenkins";
    }

    @GetMapping("/getEnv")
    public String getEnv(){
        logger.debug("getEnv");
        return environment;
    }
}
